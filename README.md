# Data table #

### Installation###

*  git clone https://miniartslondon@bitbucket.org/miniartslondon/datatable.git datatable
*  cd datatable
*  npm install


### To run the page ###

* yarn build && npx serve build -l 3000
	

### To run the page for development ###

* yarn start
	
 
### The app will run here ####
http://localhost:3000/