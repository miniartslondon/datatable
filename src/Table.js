import React, { PureComponent } from 'react';
import './Table.css';
import * as d3 from "d3";
import {individualChart} from './Chart';

export default class Table extends PureComponent {
 
    constructor(props){
        super(props);
        this.state={
          data:[]
      }
    }

    componentDidMount(){ 
      this.getData();
    }

    getData = () => { 
      let chartData= this.props.data[0];
      let key;
      let data = [];
       
       for(key in chartData){ 
           if(chartData.hasOwnProperty(key)){ 
             let num = chartData[key].values.length-1;   
                  
             data.push({'metric' : chartData[key].metric, 'value' : chartData[key].values[num], 'colour': chartData[key].colour});

           }
       }

      this.setState({
          data:[data]
      })
    }

     _drawTable = (data)=>{
        let table = d3.select('#table').append('table')
        let thead = table.append('thead')
        let	tbody = table.append('tbody');
        let columns = ["metric", "value"];

        thead.append('tr')
              .selectAll('th')
              .data(columns).enter()
              .append('th')
              .text(function (column) { return column.toUpperCase(); });

        tbody.selectAll('tr')
              .data(data)
              .enter()
              .append('tr')
              .style("border-left", function (data) {return "5px solid" + data.colour; })

              .on("mouseover", function(data){
                d3.select(this).style("background-color", data.colour);
              })
              .on("mouseout", function(){
                d3.select(this).style("background-color", "white");
              })
              .on("click", function(e, i){
                d3.event.stopPropagation();
                d3.select('#individualChart svg').remove();
                individualChart(this.props.data[0][i], i)
              }.bind(this))

              .selectAll('td')
              .data(function (row) { 
                return columns.map(function (column) {
                  return {column: column, value: row[column]};
                });
              })
              .enter()
              .append('td')
              .text(function (d) { return d.value; })
    }
    

    render() {
        if(this.state.data.length>0){ 
            return (
                <div>
                    {this._drawTable(this.state.data[0])}
                </div>
            );
        }else{
            return(
                <div>Drawing chart...</div>
            )
        }
    }
}
   