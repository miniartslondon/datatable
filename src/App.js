import React, { PureComponent } from 'react';
import './App.css';
import axios from 'axios';
import Chart  from './Chart';
import  Table  from './Table';

export default class App extends PureComponent {
  constructor(props){
    super(props);
    this.state ={
      data: []
    }
  }
  
  componentDidMount(){ 
    this.getData();
  }

  _getColours = (num) =>{
    const letters = '0123456789ABCDEF';
    let colours = []

    for(let x=0; x<num; x++){
        let colour = '#';
        for (var i = 0; i < 6; i++) {
            colour += letters[Math.floor(Math.random() * 16)];
        }
        colours.push(colour)
    }
    
    return colours;
  }

  _setColours = (data) =>{
    let colours = this._getColours(data.length);

    Object.keys(data).forEach((key, i)=>{
      Object.assign(data[key], {'colour':colours[i]});
    }) 

    return data;
  }

  _filterObj = (source, filter) => {
   const data = [];

    Object.keys(source).forEach((key, i) => { 
      if (key.indexOf(filter) === 0) {
        data.push(source[key]);
        Object.assign(source[key], {'metric': key});
      }
    });
   
    return data;
  }

  getData = () => { 
    axios
      .get('https://reference.intellisense.io/thickenernn/v1/referencia')
      .then((response) => {
        if(response.status !== 401){
          if(response.err){
            console.log('Error:', response.err);
          }else{
            let appData = response.data.current.data;
          
            const filter = "RD:647";
            let filteredData = this._filterObj(appData['pt2-scaled'], filter);
            let newData = this._setColours(filteredData);
          
            this.setState({
              data:[newData]
            })
          }
        }
        else{
          console.log('Error');
        }
      })
      .catch((error) => {
        console.error('Error: ', error)
      })
    }

  _drawTable =()=>{ 
    return <Table data={this.state.data}/> 
  }
  _drawChart = ()=>{
    return <Chart data={this.state.data}/>
  }
  render() {
    if(this.state.data.length>0){ 
      return (
        <div className="App">
          <h1>Chart with all the items</h1>
          <div id="chart"></div>
          <h1>Select a row to display an individual chart</h1>
          <div className="wrapper">
            <div id="table"></div>
            <div id="individualChart"></div>
          </div>  
            {this._drawChart()}
            {this._drawTable()}
        </div>
      );
    }
    else{
      return (
        <div className="App">
        
          Loading...
        
        </div>
        
      );
    }
  }
}

