import React, { PureComponent } from 'react';
import './Chart.css';
import * as d3 from "d3";

export default class Chart extends PureComponent {
 
    constructor(props){
        super(props);
        this.state={
            data:[]
        }
    }
    componentDidMount(){
        this.getData();
    }

    getData = () => { 
        let chartData= this.props.data[0];
        let key;
        let data = [];
        
        for(key in chartData){ 
             if(chartData.hasOwnProperty(key)){ 
                for(var i=0; i < chartData[key].values.length; i++){
                    data.push({'key' : chartData[key].metric, 'value' : chartData[key].values[i], 'time': chartData[key].times[i], 'colour': chartData[key].colour});
                }
             }
         }

        this.setState({
            data:[data]
        })
    }
  
    _showChart = function() {
        const margin = {top: 10, right: 30, bottom: 30, left: 60},
            width = 600 - margin.left - margin.right,
            height = 600 - margin.top - margin.bottom,
            data = this.state.data[0];

        let svg = d3.select("#chart")
                    .append("svg")
                        .attr("width", width + margin.left + margin.right)
                        .attr("height", height + margin.top + margin.bottom)
                    .append("g")
                        .attr("transform", `translate(${margin.left},${margin.top})`)
 
        let sumstat = d3.nest() 
                        .key(function(d) { return d.key;})
                        .entries(data);
 
        let xScale = d3.scaleLinear()
                    .domain(d3.extent(data, dataPoint => dataPoint.time)) //d3.extent(data, function(d) {return d.time; })
                    .range([ 0, width ]);

            svg.append("g")
                .attr("transform", `translate(0,${height})`)
                .call(d3.axisBottom(xScale).ticks(10));
                
        let yScale = d3.scaleLinear()
                    .domain([0, d3.max(data, dataPoint => dataPoint.value)])
                    .range([ height, 0 ]);
            
            svg.append("g")
                    .call(d3.axisLeft(yScale));

        svg.selectAll(".line")
            .data(sumstat)
            .enter()
            .append("path")
                .attr("fill", "none")
                .attr("stroke", function(d){ return d.values[0].colour })
                .attr("stroke-width", 1.5)
                .attr("d", function(d){
                    return d3.line()
                            .x(function(d) { return xScale(d.time); })
                            .y(function(d) { return yScale(+d.value); })
                            (d.values)
                })
          
    }

    render() {
        if(this.state.data.length>0){ //console.log(this.state.data)
            return (
                <div>
                    {this._showChart()}
                </div>
            );
        }else{
            return(
                <div>Drawing chart...</div>
            )
        }
    }
}
   

export const getData = (data, i) => { 
    let chartData= data;
  
    let newData = [];
        for(let i=0; i < chartData.values.length; i++){
            newData.push({'value' : chartData.values[i], 'time': chartData.times[i], 'colour': chartData.colour});
        }
  
    return newData;
}

export const  individualChart = (item ,i) => { 

    let data = getData(item),
        colour = data[0].colour;
    
    const margin = {top: 10, right: 30, bottom: 30, left: 60};

    const width = 600 - margin.left - margin.right;
    const height = 300 - margin.top - margin.bottom;
    
    const svg = d3.select("#individualChart")
                    .append("svg")
                        .attr("height", height + margin.top + margin.bottom)
                        .attr("width", width + margin.left + margin.right);
    
    const chart = svg.append("g")
                    .attr("transform", `translate(${margin.left},${margin.top})`)

    const yScale = d3.scaleLinear()
                    .domain([d3.min(data, dataPoint => dataPoint.value), d3.max(data, dataPoint => dataPoint.value)])
                    .range([height, 0]);
                    
                    
    const xScale = d3.scaleLinear()
                    .domain(d3.extent(data, dataPoint => dataPoint.time))
                    .range([0, width]);
                    
    const line = d3.line()
                    .x(dataPoint => xScale(dataPoint.time))
                    .y(dataPoint => yScale(dataPoint.value));

    const path =  chart.append("g")
                            .attr("transform", `translate(-${margin.left},-${margin.top})`)
                        .append("path")
                            .attr("transform", `translate(${margin.left},0)`)
                        .datum(data)
                            .attr("fill", "none")
                            .attr("stroke", colour)
                            .attr("stroke-linejoin", "round")
                            .attr("stroke-linecap", "round")
                            .attr("stroke-width", 1.5)
                            .attr("d", line);

    const pathLength = path.node().getTotalLength();

    const transitionPath = d3.transition()
                            .ease(d3.easeSin)
                            .duration(2500);

    path.attr("stroke-dashoffset", pathLength)
        .attr("stroke-dasharray", pathLength)
        .transition(transitionPath)
        .attr("stroke-dashoffset", 0);

    chart.append("g")
        .attr("transform", `translate(0,${height})`)
        .call(d3.axisBottom(xScale).ticks(data.length));

    chart.append("g")
        .attr("transform", `translate(0, 0)`)
        .call(d3.axisLeft(yScale));

}